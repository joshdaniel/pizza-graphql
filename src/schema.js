/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import {
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

/* ---------------------------------------- */
/*                App modules               */
/* ---------------------------------------- */
import pizza       from './query-types/pizza.js';
import pizzaList   from './query-types/pizzaList.js';
import toppingList from './query-types/toppingList.js';
import orders      from './query-types/orders.js';
import order       from './query-types/order.js';
import orderPlace  from './query-types/orderPlace.js';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */

/**
 * Export GraphQL schema.
 *
 * @param  {Object} models
 * @return {Object}
 */
export default function (models) {
    // Define the query types
    const queryType = new GraphQLObjectType({
        name   : 'Query',
        fields : {
            pizza    : pizza(models),
            pizzas   : pizzaList(models),
            toppings : toppingList(models),
            orders   : orders(models),
            order    : order(models),
        },
    });

    // Define the mutation types
    const mutationType = new GraphQLObjectType({
        name   : 'Mutation',
        fields : {
            placeOrder: orderPlace(models),
        },
    });

    return new GraphQLSchema({
        query    : queryType,
        mutation : mutationType,
    });
}
