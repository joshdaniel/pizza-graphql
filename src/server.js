/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import express     from 'express';
import graphqlHTTP from 'express-graphql';
import Sequelize   from 'sequelize';

/* Load env file in dev mode. */
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').load();
}

/* ---------------------------------------- */
/*                App modules               */
/* ---------------------------------------- */

import models from '../database/models/index.js';
import schema from './schema.js';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */

/* Configure database connection */
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host    : process.env.DB_HOST,
    dialect : 'mysql',

    pool: {
        max     : 5,
        min     : 0,
        acquire : 30000,
        idle    : 10000
    },

    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    operatorsAliases: false
});

/* Initialise express */
const app = express();

/* Server port to listen on */
const PORT = process.env.PORT;

/* Pass sequelize to models */
const sequelModels = models(sequelize);

/* Middleware between Express and GraphQL */
app.use('/graphql', graphqlHTTP({
    schema   : schema(sequelModels),
    graphiql : true,
}));

/**
 * Connect and sync database before starting the server.
 */
sequelModels.sequelize.sync({ force: false }).then(() => {
    app.listen(PORT, () => {
        process.stdout.write(`Now browse to localhost:${PORT}/graphql`);
    });
}).catch(function (err) {
    process.stdout.write('Error syncing with database');
    process.stdout.write(err);
});
