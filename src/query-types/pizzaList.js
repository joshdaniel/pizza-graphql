/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import {
    GraphQLList,
    GraphQLInt
} from 'graphql';

/* ---------------------------------------- */
/*                App modules               */
/* ---------------------------------------- */
import { pizzaType } from './pizza.js';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
export const pizzaListType = new GraphQLList(pizzaType);

/**
 * Pizza list query type.
 *
 * @param  {Object} models
 * @return {Object}
 */
export default function (models) {
    return {
        type : pizzaListType,
        args : {
            limit: { type: GraphQLInt }
        },
        resolve(_, { limit }) {
            const options = {
                limit,
            };

            return models.pizza.findAll(options);
        },
    };
}
