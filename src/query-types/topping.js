/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
export const toppingType = new GraphQLObjectType({
    name   : 'Topping',
    fields : {
        id   : { type: GraphQLString },
        name : { type: GraphQLString },
    }
});

/**
 * Topping query type.
 *
 * @param  {Object} topping
 * @return {Object}
 */
export default function (topping) {
    return {
        type: toppingType,
        resolve () {
            return topping.findAll({});
        },
    };
}
