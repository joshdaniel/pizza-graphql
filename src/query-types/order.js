/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLFloat
} from 'graphql';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
export const orderType = new GraphQLObjectType({
    name   : 'Order',
    fields : {
        id        : { type: GraphQLString },
        subTotal  : { type: GraphQLFloat },
        vat       : { type: GraphQLFloat },
        total     : { type: GraphQLFloat },
        createdAt : { type: GraphQLString }
    },
});

/**
 * Order query type.
 *
 * @param  {Object} models
 * @return {Object}
 */
export default function (models) {
    return {
        type : orderType,
        args : {
            id: { type: GraphQLString }
        },
        resolve(_, { id }) {
            return models.order.findByPk(id);
        }
    };
}
