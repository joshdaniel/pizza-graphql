/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLFloat
} from 'graphql';

/* ---------------------------------------- */
/*                App modules               */
/* ---------------------------------------- */
import { toppingListType } from './toppingList';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
export const pizzaType = new GraphQLObjectType({
    name   : 'Pizza',
    fields : {
        id       : { type: GraphQLString },
        name     : { type: GraphQLString },
        price    : { type: GraphQLFloat },
        toppings : {
            type: toppingListType,
            resolve(pizza) {
                return pizza.getToppings();
            }
        },
    },
});

/**
 * Pizza schema definition.
 *
 * @param  {Object} pizza
 * @return {Object}
 */
export default function (models) {
    return {
        type : pizzaType,
        args : {
            id: { type: GraphQLString }
        },
        resolve (_, { id }) {
            return models.pizza.findByPk(id);
        },
    };
}
