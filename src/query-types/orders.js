/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import {
    GraphQLList,
    GraphQLInt
} from 'graphql';

/* ---------------------------------------- */
/*                App modules               */
/* ---------------------------------------- */
import { orderType } from './order.js';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
export const orderListType = new GraphQLList(orderType);

/**
 * Pizza list query type.
 *
 * @param  {Object} models
 * @return {Object}
 */
export default function (models) {
    return {
        type : orderListType,
        args : {
            limit: { type: GraphQLInt },
        },
        resolve(_, { limit }) {
            return models.order.findAll({
                limit
            });
        },
    };
}
