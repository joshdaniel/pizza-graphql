/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import { GraphQLList } from 'graphql';

/* ---------------------------------------- */
/*                App modules               */
/* ---------------------------------------- */
import { toppingType } from './topping.js';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
export const toppingListType = new GraphQLList(toppingType);

/**
 * Topping list query type.
 *
 * @param  {Object} models
 * @return {Object}
 */
export default function (models) {
    return {
        type: toppingListType,
        resolve() {
            return models.topping.findAll({});
        },
    };
}
