/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
import { Op } from 'sequelize';
import {
    GraphQLList,
    GraphQLString
} from 'graphql';

/* ---------------------------------------- */
/*                App modules               */
/* ---------------------------------------- */
import { orderType } from './order.js';

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */

/**
 * Order place query type.
 *
 * @param  {Object} models
 * @return {Object}
 */
export default function (models) {
    return {
        type : orderType,
        args : {
            pizzaIds: { type: new GraphQLList(GraphQLString) },
        },
        async resolve(_, { pizzaIds }) {
            const returnOrder = {
                subTotal : 0,
                vat      : 0,
                total    : 0,
            };

            try {
                const pizzas = await models.pizza.findAll({
                    where: {
                        id: {
                            [Op.in]: pizzaIds
                        }
                    }
                });

                if (pizzas.length < pizzaIds.length) {
                    return returnOrder;
                }

                pizzas.forEach(pizza => {
                    returnOrder.subTotal += parseFloat(pizza.get('price'));
                });

                returnOrder.vat = returnOrder.subTotal * 0.2;
                returnOrder.total = returnOrder.subTotal * 1.2;

                return models.order.create({
                    subTotal : returnOrder.subTotal,
                    vat      : returnOrder.vat,
                    total    : returnOrder.total,
                });
            } catch (err) {
                return returnOrder;
            }
        }
    };
}
