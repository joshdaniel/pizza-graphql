'use strict';

/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
const moment = require('moment');
const uuidv4 = require('uuid/v4');

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
module.exports = {
    up: queryInterface => {
        let toppings = [
            {
                name: 'Red peppers',
            },
            {
                name: 'Green peppers',
            },
            {
                name: 'Cheese',
            },
            {
                name: 'Ham',
            },
            {
                name: 'Mozzarella',
            },
            {
                name: 'Tomato',
            },
            {
                name: 'Meatballs',
            },
            {
                name: 'Mushrooms',
            },
            {
                name: 'Olives',
            },
            {
                name: 'Broccoli',
            },
            {
                name: 'Spinach',
            },
            {
                name: 'Sweet corn',
            },
            {
                name: 'Avocado',
            },
            {
                name: 'Pineapple',
            },
            {
                name: 'BBQ Chicken',
            },
            {
                name: 'Turkey',
            },
            {
                name: 'Pepperoni',
            },
            {
                name: 'Chorizo',
            },
            {
                name: 'Bacon',
            },
            {
                name: 'Red onions',
            },
            {
                name: 'Garlic sauce',
            },
            {
                name: 'BBQ sauce',
            },
        ];

        toppings = toppings.map(topping => {
            const now = moment.utc().format('YYYY-MM-DD hh:mm:ss');

            topping.id        = uuidv4();
            topping.createdAt = now;
            topping.updatedAt = now;

            return topping;
        });

        return queryInterface.bulkInsert('toppings', toppings, {});
    },

    down: queryInterface => {
        return queryInterface.bulkDelete('toppings', null, {});
    }
};
