'use strict';

/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
const path   = require('path');
const moment = require('moment');

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
module.exports = {
    up: queryInterface => {
        const pizza = queryInterface.sequelize.import(
            path.resolve('database', 'models', 'pizza.js')
        );
        const topping = queryInterface.sequelize.import(
            path.resolve('database', 'models', 'topping.js')
        );

        const pizzaToppings = {
            'BBQ Feast': [
                'Bacon',
                'Chicken',
                'Red onions',
                'Green peppers',
                'Red peppers',
            ],
            'Veggie Supreme': [
                'Red onions',
                'Green peppers',
                'Red peppers',
                'Mushrooms',
                'Tomato',
                'Spinach',
                'Sweet corn',
            ],
            'Meat House': [
                'Chicken',
                'Pepperoni',
                'Chorizo',
                'Bacon',
                'Green peppers',
                'Red peppers',
                'Red onions',
            ],
            'BBQ Chicken': [
                'BBQ sauce',
                'Chicken',
                'Mushrooms',
                'Sweet corn'
            ],
            'Italian Veg': [
                'Olives',
                'Tomato',
                'Red onions',
                'Mushrooms',
                'Green peppers',
                'Red peppers',
                'Garlic sauce',
            ],
            'Hawaiian Delish': [
                'Ham',
                'Pineapple',
                'Mushrooms',
            ],
        };

        return pizza.findAll().then(async pizzas => {
            return {
                pizzas,
                toppings: await topping.findAll(),
            };
        }).then(data => {
            const bulkData = [];

            // Loop through each pizza
            data.pizzas.forEach(pizzaModel => {
                const pizzaName = pizzaModel.get('name');

                data.toppings.forEach(toppingModel => {
                    const toppingName   = toppingModel.get('name');
                    const toppingsArray = pizzaToppings[pizzaName];

                    if (typeof toppingsArray === 'undefined') {
                        return;
                    }

                    if (toppingsArray.indexOf(toppingName) < 0) {
                        return;
                    }

                    const now = moment.utc().format('YYYY-MM-DD hh:mm:ss');

                    bulkData.push({
                        pizzaId   : pizzaModel.get('id'),
                        toppingId : toppingModel.get('id'),
                        createdAt : now,
                        updatedAt : now,
                    });
                });
            });

            queryInterface.bulkInsert('pizza_toppings', bulkData);
        });
    },

    down: queryInterface => {
        return queryInterface.bulkDelete('pizza_toppings', null, {});
    }
};
