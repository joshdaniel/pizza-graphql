'use strict';

/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
const moment = require('moment');
const uuidv4 = require('uuid/v4');

/* ---------------------------------------- */
/*                   Main                   */
/* ---------------------------------------- */
module.exports = {
    up: queryInterface => {
        let pizzas = [
            {
                name  : 'BBQ Feast',
                price : 12.99,
            },
            {
                name  : 'Veggie Supreme',
                price : 12.99,
            },
            {
                name  : 'Meat House',
                price : 9.99
            },
            {
                name  : 'BBQ Chicken',
                price : 9.99
            },
            {
                name  : 'Italian Veg',
                price : 7.99
            },
            {
                name  : 'Hawaiian Delish',
                price : 7.99
            },
        ];

        pizzas = pizzas.map(pizza => {
            const now = moment.utc().format('YYYY-MM-DD hh:mm:ss');

            pizza.id        = uuidv4();
            pizza.createdAt = now;
            pizza.updatedAt = now;

            return pizza;
        });

        return queryInterface.bulkInsert('pizzas', pizzas, {});
    },

    down: queryInterface => {
        return queryInterface.bulkDelete('pizzas', null, {});
    }
};
