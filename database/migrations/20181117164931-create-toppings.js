'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('toppings', {
            id: {
                type          : Sequelize.UUID,
                primaryKey    : true,
                autoIncrement : false,
                defaultValue  : Sequelize.UUIDV4,
            },
            name: {
                type      : Sequelize.STRING,
                allowNull : false,
            },
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE,
        });
    },

    down: queryInterface => {
        return queryInterface.dropTable('toppings');
    }
};
