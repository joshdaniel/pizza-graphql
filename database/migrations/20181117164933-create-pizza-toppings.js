'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('pizza_toppings', {
            id: {
                type          : Sequelize.INTEGER,
                primaryKey    : true,
                autoIncrement : true,
            },
            pizzaId: {
                type      : Sequelize.UUID,
                allowNull : false,
            },
            toppingId: {
                type      : Sequelize.UUID,
                allowNull : false,
            },
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE,
        });
    },

    down: queryInterface => {
        return queryInterface.dropTable('pizza_toppings');
    }
};
