'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('pizzas', {
            id: {
                type          : Sequelize.UUID,
                primaryKey    : true,
                autoIncrement : false,
                defaultValue  : Sequelize.UUIDV4,
            },
            name: {
                type      : Sequelize.STRING,
                allowNull : false,
            },
            price: {
                type         : Sequelize.DECIMAL(10, 2),
                defaultValue : 0,
                allowNull    : false,
            },
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE,
        });
    },

    down: queryInterface => {
        return queryInterface.dropTable('pizzas');
    }
};
