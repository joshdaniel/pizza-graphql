'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('orders', {
            id: {
                type          : Sequelize.UUID,
                primaryKey    : true,
                autoIncrement : false,
                defaultValue  : Sequelize.UUIDV4,
            },
            subTotal: {
                type         : Sequelize.DECIMAL(10, 2),
                defaultValue : 0,
                allowNull    : false,
            },
            vat: {
                type         : Sequelize.DECIMAL(10, 2),
                defaultValue : 0,
                allowNull    : false,
            },
            total: {
                type         : Sequelize.DECIMAL(10, 2),
                defaultValue : 0,
                allowNull    : false,
            },
            createdAt : Sequelize.DATE,
            updatedAt : Sequelize.DATE,
        });
    },

    down: queryInterface => {
        return queryInterface.dropTable('orders');
    }
};
