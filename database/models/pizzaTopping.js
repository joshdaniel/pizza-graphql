/**
 * Export pizza model.
 *
 * @param  {Object} sequelize
 * @param  {Object} DataTypes
 * @return {Object}
 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('pizza_topping', {
        id: {
            type          : DataTypes.INTEGER,
            primaryKey    : true,
            autoIncrement : true,
        },
        pizzaId: {
            type      : DataTypes.UUID,
            allowNull : false,
        },
        toppingId: {
            type      : DataTypes.UUID,
            allowNull : false,
        },
    });
};
