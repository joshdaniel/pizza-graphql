/**
 * Export pizza model.
 *
 * @param  {Object} sequelize
 * @param  {Object} DataTypes
 * @return {Object}
 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('pizza', {
        id: {
            type          : DataTypes.UUID,
            primaryKey    : true,
            autoIncrement : false,
            defaultValue  : DataTypes.UUIDV4,
        },
        name: {
            type      : DataTypes.STRING,
            allowNull : false,
        },
        price: {
            type         : DataTypes.DECIMAL(10, 2),
            defaultValue : 0,
            allowNull    : false,
        },
    });
};
