/**
 * Export topping model.
 *
 * @param  {Object} sequelize
 * @param  {Object} DataTypes
 * @return {Object}
 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('topping', {
        id: {
            type          : DataTypes.UUID,
            primaryKey    : true,
            autoIncrement : false,
            defaultValue  : DataTypes.UUIDV4,
        },
        name: {
            type      : DataTypes.STRING,
            allowNull : false,
        },
    });
};
