/**
 * Export order model.
 *
 * @param  {Object} sequelize
 * @param  {Object} DataTypes
 * @return {Object}
 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('order', {
        id: {
            type          : DataTypes.UUID,
            primaryKey    : true,
            autoIncrement : false,
            defaultValue  : DataTypes.UUIDV4,
        },
        subTotal: {
            type         : DataTypes.DECIMAL(10, 2),
            defaultValue : 0,
            allowNull    : false,
        },
        vat: {
            type         : DataTypes.DECIMAL(10, 2),
            defaultValue : 0,
            allowNull    : false,
        },
        total: {
            type         : DataTypes.DECIMAL(10, 2),
            defaultValue : 0,
            allowNull    : false,
        },
    });
};
