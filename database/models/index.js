/* ---------------------------------------- */
/*                NPM modules               */
/* ---------------------------------------- */
const path = require('path');

/**
 * Export sequelize and models.
 *
 * @param  {Object} sequelize
 * @return {Object}
 */
module.exports = function (sequelize) {
    const obj = {
        sequelize
    };

    // Models to be imported
    const modelNames = [
        'pizza',
        'topping',
        'pizzaTopping',
        'order',
    ];

    // Loop through and import models.
    modelNames.forEach(name => {
        obj[name] = sequelize.import(path.resolve(__dirname, `${name}.js`));
    });

    obj.pizza.belongsToMany(obj.topping, {
        through: {
            model  : obj.pizzaTopping,
            unique : false,
        },
    });

    return obj;
};
