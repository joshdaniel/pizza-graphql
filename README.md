# Pizza GraphQL

This project aims to use GraphQL with MySQL.

## Local

You can run the project locally using the following commands.

**For Yarn**
`yarn dev`

** For NPM**
`npm run dev`

## Build

You can also build the project. This will convert the ES6 code to pure JavaScript.

`npm run build`

## Database

When starting the server sequelize will handle creating the database tables.

You can seed the database by running the following command.

`node_modules/.bin/sequelize db:seed:all`
